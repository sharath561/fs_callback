//1. Read the given file lipsum.txt
//2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

const fs = require("fs");
function readTheFile() {
  fs.readFile("../lipsum_1.txt", "utf-8", (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log(data);
      let upperCaseData = data.toUpperCase();
      fs.writeFile(
        "./upperCase.txt",
        JSON.stringify(upperCaseData),
        "utf-8",
        (error, data) => {
          if (error) {
            console.log(error);
          } else {
            console.log("upperCase.txt file is created successfully");
            fs.writeFile("./filenames.txt", "upperCase.txt", (error) => {
              if (error) {
                console.log(error);
              } else {
                console.log(
                  "upperCase.txt file name is store in filenames.txt"
                );
                fs.readFile("./upperCase.txt", "utf-8", (error, data) => {
                  if (error) {
                    console.log(error);
                  } else {
                    let lowerCaseData = data.toLowerCase().split(".");
                    console.log(
                      "chnaged the text in lowerCase and file is created"
                    );

                    fs.writeFile(
                      "./lowerCase.txt",
                      JSON.stringify(lowerCaseData),
                      (error) => {
                        if (error) {
                          console.log(error);
                        } else {
                          console.log(
                            "lowerCase.txt file is stored in filenames.txt"
                          );
                          fs.appendFile(
                            "./filenames.txt",
                            "\nlowerCase.txt",
                            (error) => {
                              if (error) {
                                console.log(error);
                              } else {
                                let sortedData = lowerCaseData.sort();
                                console.log("The data is sorted successfully");
                                fs.writeFile(
                                  "./sortedContentData.txt",
                                  JSON.stringify(sortedData),
                                  "utf-8",
                                  (error) => {
                                    if (error) {
                                      console.log(error);
                                    } else {
                                      console.log(
                                        "sortedContentData.txt file name is stored in filenames.txt"
                                      );
                                      fs.appendFile(
                                        "./filenames.txt",
                                        "\nsortedContentData.txt",
                                        (error) => {
                                          if (error) {
                                            console.log(error);
                                          } else {
                                            fs.readFile(
                                              "./filenames.txt",
                                              "utf-8",
                                              (error, data) => {
                                                if (error) {
                                                  console.log(error);
                                                } else {
                                                  console.log(
                                                    "reading the filenames.txt "
                                                  );

                                                  let iterateData =
                                                    data.split("\n");
                                                  for (let item of iterateData) {
                                                    fs.unlink(
                                                      `./${item}`,
                                                      (error) => {
                                                        if (error) {
                                                          console.log(error);
                                                        } else {
                                                          console.log(
                                                            `${item} is deleted successfully`
                                                          );
                                                        }
                                                      }
                                                    );
                                                  }
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            }
                          );
                        }
                      }
                    );
                  }
                });
              }
            });
          }
        }
      );
    }
  });
}

module.exports = readTheFile;
