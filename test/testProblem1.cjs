const createRandomFiles = require("../problem1.cjs");
const fs = require("fs");

function createFileCallBack(filesPath, deleteFileCallBack) {
  fs.writeFile(filesPath, "sharath kumar", "utf-8", (error) => {
    if (error) {
      console.log(error);
    } else {
      console.log("File is created successfully.");
      deleteFileCallBack(filesPath);
    }
  });
}

function deleteFileCallBack(filesPath) {
  fs.unlink(filesPath, (error) => {
    if (error) {
      console.log(error);
    } else {
      console.log("File is deleted succesfully");
    }
  });
}
createRandomFiles(createFileCallBack, deleteFileCallBack);
