const fs = require("fs");
function createRandomFiles(createFileCallBack, deleteFileCallBack) {
  let folderName = "random";

  let filesData = [
    `../${folderName}/random1.json`,
    `../${folderName}/random2.json`,
    `../${folderName}/random3.json`,
  ];

  if (!fs.existsSync(folderName)) {
    //console.log("here")
    fs.mkdir(`../${folderName}`, (error) => {
      if (error) {
        console.log(error);
      }
    });
  }

  for (let filesPath of filesData) {
    createFileCallBack(filesPath, deleteFileCallBack);
  }
}

module.exports = createRandomFiles;
